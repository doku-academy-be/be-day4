import java.util.*;

public class problem2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        var size = input.length();

        var list1 = new ArrayList<>();
        var result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
           var c = input.charAt(i);

            list1.add(c);
        }
        for (int i = 0; i < list1.size() ; i++) {

            boolean unique = true;
            for (int j = 0; j < list1.size() ; j++) {

                if(i==j){

                    continue;

                }

                if (list1.get(i)==list1.get(j)){

                    unique = false;
                    break;
                }

            }
            if (unique){

                result.add(list1.get(i));
            }
        }
        System.out.println(result);



    }
}
